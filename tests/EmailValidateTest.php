<?php


namespace TddFeatures\Examples\Testing;


use PHPUnit\Framework\TestCase;
use TddFeatures\Examples\EmailValidator;

class EmailValidateTest extends TestCase
{

    public function testValidateEmail()
    {
        $email1 = 'zakariaazaraf@gmail.com';
        $email2 = 'zakaria.gmail.com';
        $email3 = '{{{zakaria@claromentis.com';


        $emailValidator = new EmailValidator();

        $isValidEmail = $emailValidator->isValidEmail($email1);
        $this->assertEquals(true, $isValidEmail, "$email1 expected to be Valid");

        $isValidEmail = $emailValidator->isValidEmail($email2);
        $this->assertEquals(false, $isValidEmail, "$email2 expected to be inValid");

        $isValidEmail = $emailValidator->isValidEmail($email3);
        $this->assertEquals(true, $isValidEmail, "$email3 expected to be inValid");
    }
}