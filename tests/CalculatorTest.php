<?php


namespace TddFeatures\Examples\Testing;


use PHPUnit\Framework\TestCase;
use TddFeatures\Examples\Calculator;

class CalculatorTest extends TestCase
{

    /** Given */
    /**
     * @Description: The client want a software/app to handle his arithmetics operations in the 'Store'
     * * @Senario1: Additional Operation for 'Prices'
     * * @Senario1: Subtraction Operation for 'Prices'
     * * @Senario1: Multiplication Operation for 'Prices'
     * * @Senario1: Division Operation for 'Prices'
     */


    /** @test */
    public function addition()
    {
        /** @Given Additional Operation for 'Prices' function */

        /** When (actions) */
        $calculator = new Calculator();

        $price1 = 20;
        $price2 = 5;
        $expectedResult = 25;

        $total = $calculator->priceAddition($price1, $price2);

        /** Then (assertions) */
        $this->assertEquals($expectedResult, $total, "The Calculated Total should be $expectedResult");


        /** When (actions) */
        $price1 = 42;
        $price2 = 7;
        $expectedResult = 49;

        $total = $calculator->priceAddition($price1, $price2);

        /** Then (assertions) */
        $this->assertEquals($expectedResult, $total, "The Calculated Total should be $expectedResult");

    }


    /**
     * @group failing
     * @test
     */
    public function FixBugAdditionPriceFloatType()
    {
        /** @Give The Test fixes the issue, when we use float args */

        /** @When Perform those actions */
        $calculator = new Calculator();

        $price1 = 17.5;
        $price2 = 3.25;
        $expectedResult = 20.75;

        $total = $calculator->priceAddition($price1, $price2);

        $this->assertEquals($expectedResult, $total, "The Expected Result Should be : $expectedResult");
        $this->assertIsFloat($total, 'The expected returned type should be "float"');

        $price1 = -17.5;
        $price2 = 3.25;
        $expectedResult = null;

        $total = $calculator->priceAddition($price1, $price2);


        $this->assertEquals($expectedResult, $total, "The Expected Result Should be : $expectedResult");
        $this->assertNull($total, 'The expected returned type should be "float"');

        $price1 = 0.5;
        $price2 = 10.00;
        $expectedResult = 10.5;

        $total = $calculator->priceAddition($price1, $price2);

        $this->assertEquals($expectedResult, $total, "The Expected Result Should be : $expectedResult");
        $this->assertIsFloat($total, 'The expected returned type should be "float"');

        $price1 = 'aa';
        $price2 = 10.00;
        $expectedResult = null;

        $total = $calculator->priceAddition($price1, $price2);

        $this->assertEquals($expectedResult, $total, "The Expected Result Should be : $expectedResult");
        $this->assertNull($total, 'The expected returned type should be "float"');
    }
}