<?php


namespace TddFeatures\Examples\Testing;


use PHPUnit\Framework\TestCase;
use TddFeatures\Examples\ValidateIPAddress;


class ValidateIPAddressTest extends TestCase
{

    /**
     * @Desc Test the validate ip address feature
     */
    public function testValidateIPAddress()
    {
        $IPAddressValidator = new ValidateIPAddress();

        $validIPsAddresses = [
            '69.162.124.227',
            '69.162.124.233',
            '69.162.124.237',
            '216.245.221.84',
            '216.245.221.93',
            '2001:db8::2:1'
        ];


        $inValidIPsAddresses = [
            '69.16"2.124.227',
            '69.162.124.233.',
            '.69.162.124.237',
            '21622.245.221.84',
            '216.2t45.221.93',
            '2001:db8::2:q1'
        ];

        /** Validate Valid IPs */
        foreach($validIPsAddresses as $validIPAddress){

            $isValidIPAddress = $IPAddressValidator->isValideIPAddress($validIPAddress);
            $this->assertEquals(true, $isValidIPAddress, "$validIPAddress Expected to be 'Valid'");
        }

        /** Validate InValid IPs */
        foreach($inValidIPsAddresses as $InValidIPAddress){

            $isValidIPAddress = $IPAddressValidator->isValideIPAddress($InValidIPAddress);
            $this->assertEquals(false, $isValidIPAddress, "$InValidIPAddress Expected to be 'InValid'");
        }



    }

    /**
     * @Desc Test the allowed ips addresses feature
     */
    public function testAllowedIPAddress()
    {
        $validateIPs = new ValidateIPAddress();

        $allowedIPsAddresses = [
            '69.162.124.227',
            '69.162.124.233',
            '69.162.124.237',
            '138.197.150.151',
            '216.245.221.84',
            '216.245.221.93',
		    '2001:db8::2:1',
            '2a03:b0c0:0:1010::832:1',
            '2604:a880:800:10::4e6:f001'
        ];

        $whiteListIPs = [
            '2a03:b0c0:0:1010::832:1',
            '2001:db8::2:1',
            '216.245.221.84',
            '138.197.150.151',
            '69.162.124.233',
            '2604:a880:800:10::4e6:f001'
        ];

        $blackListIPs = [
            '122.248.234.23',
            '128.199.195.156',
            '139.59.173.249',
            '146.185.143.14',
            '54.94.142.218',
            '2a03:b0c0:1:d0::e54:a001'
        ];


        /** Test The Allowed IPS */
        foreach($whiteListIPs as $allowedIP){
            $isAllowedIP = $validateIPs->isAllowedIP($allowedIPsAddresses, $allowedIP);
            $this->assertEquals(true, $isAllowedIP, "This IP: $allowedIP Expected to be 'Allowed!'");
        }

        /** Test Black List IPs */
        foreach($blackListIPs as $blockedIP){
            $isAllowedIP = $validateIPs->isAllowedIP($allowedIPsAddresses, $blockedIP);
            $this->assertEquals(false, $isAllowedIP, "This IP: $blockedIP Expeced to be 'Blocked'");
        }
    }
}