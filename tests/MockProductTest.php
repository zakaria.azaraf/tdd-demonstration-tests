<?php


namespace TddFeatures\Examples\Testing;


use PHPUnit\Framework\TestCase;
use TddFeatures\Examples\ProductRepository;

class MockProductTest extends TestCase
{

    /** @test */
    public function isProductReturned()
    {
        $mockProductRepository = $this->createMock(ProductRepository::class);

        $mockProduct = [
            [
                'id' => 2,
                'name' => 'product 1',
                'description' => 'product 1 description',
                'price' => '222$'
            ],
            [
                'id' => 222,
                'name' => 'product 122',
                'description' => 'product 2221 description',
                'price' => '22$'
            ]
        ];

        $mockProductRepository->method('getAllProduct')->willReturn($mockProduct);

        $products = $mockProductRepository->getAllProduct();

        $this->assertEquals('product 1', $products[0]['name']);
        $this->assertEquals('product 122', $products[1]['name']);

        $this->assertEquals('22$', $products[1]['price']);

    }

    /**
     * @Description: Test if the insertion works fine
     * @test
     */
    public function isProductInserted()
    {
        $productRepository = new ProductRepository();

        $isInserted = $productRepository->insertProduct("phone", "this the description of the phone", "342$");

        $this->assertTrue($isInserted);

        $isInserted = $productRepository->insertProduct( 'ball', 'this the description of the ball', '31$');

        $this->assertTrue($isInserted);
    }

    /**
     * @Description: This test function ensures that the truncate table is deleted successfully
     * @group truncateTable
     * @test
     */

    public function isTableTruncated()
    {
        /**
         * @Given: This Function is for testing that the truncate feature works fine.
         */

        /** @When: This test executed and passed, It ensures that the functionality works as needed */
        $productRepository = new ProductRepository();

        $tableName = 'products';

        $isTruncated = $productRepository->truncateTable($tableName);
        $this->assertTrue($isTruncated, "The test should pass!");

        $tableName = 'nothing';

        $isTruncated = $productRepository->truncateTable($tableName);
        $this->assertFalse($isTruncated, "The test table $tableName should Fail!");

        $tableName = 222222;

        $isTruncated = $productRepository->truncateTable($tableName);
        $this->assertFalse($isTruncated, "The test table $tableName should Fail!");

        $tableName = '###{[|||';

        $isTruncated = $productRepository->truncateTable($tableName);
        $this->assertFalse($isTruncated, "The test table $tableName should Fail!");
    }
}