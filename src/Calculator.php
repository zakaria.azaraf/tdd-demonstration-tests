<?php


namespace TddFeatures\Examples;


class Calculator
{

    /**
     * Calculator constructor.
     */
    public function __construct()
    {
    }

    /** @Desc This method calculate the some of two values */
    public function priceAddition($price1, $price2)
    {
        $isValidPrice1 = is_numeric($price1) && !is_string($price1) && $price1 >= 0;
        $isValidPrice2 = is_numeric($price2) && !is_string($price2) && $price2 >= 0;

        if($isValidPrice1 && $isValidPrice2){
            return $price1 + $price2;
        }

        return null;
    }
}