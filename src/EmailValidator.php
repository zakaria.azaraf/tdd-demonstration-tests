<?php


namespace TddFeatures\Examples;


class EmailValidator
{

    /**
     * EmailValidator constructor.
     */
    public function __construct()
    {
    }

    public function isValidEmail(string $email = '') : bool
    {
        return (bool)filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}