<?php


namespace TddFeatures\Examples;


class ValidateIPAddress
{

    /**
     * ValidateIPAddress constructor.
     */
    public function __construct()
    {
    }

    public function isValideIPAddress(string $ip) : bool
    {
        return (bool)filter_var($ip, FILTER_VALIDATE_IP);
    }

    /**
     *  @Desc Test if the IP allowed
     *  @param array List of Allowed IPs
     *  @param string IP Address
     */
    public function isAllowedIP(array $allowedIPs, string $IP) : bool
    {
        return in_array($IP, $allowedIPs);
    }
}