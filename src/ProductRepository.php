<?php


namespace TddFeatures\Examples;

class ProductRepository
{
    protected $pdo;

    protected function getPdo() : \PDO
    {
        if($this->pdo === null)
        {
            $options = [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            ];

            try
            {
                $this->pdo = new \PDO("mysql:host=localhost;dbname=phpunit-demo;charset=utf8mb4", 'root', '', $options);

            }catch(\PDOException $e)
            {
                throw new \PDOException($e->getMessage(), $e->getCode());
            }
        }

        return $this->pdo;
    }

    public function getAllProduct() : array
    {
        return $this->getPdo()->prepare("Select * from products")->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function insertProduct($name, $description, $price): bool
    {
        $query = "INSERT INTO `products` (`id`, `name`, `description`, `price`) VALUES (NULL, '$name', '$description', '$price')";

        try{
            $this->getPdo()->prepare($query)->execute();
        }catch(\PDOException $e)
        {
            return false;
        }

        return true;
    }

    /**
     * @Description: The function truncate a given table
     * @param string
     */
    public function truncateTable($tableName)
    {
        $isTableValid = is_string($tableName) && !is_numeric($tableName);

        if(!$isTableValid){
            return false;
        }

        try{
            $this->getPdo()->prepare("TRUNCATE TABLE $tableName")->execute();
        }catch(\PDOException $e)
        {
            return false;
        }
        return true;
    }
}